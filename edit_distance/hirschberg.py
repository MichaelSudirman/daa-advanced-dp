import sys
from math import floor
from multiprocessing import Pool
import time

from lcs import get_last_row_lcs_table, lcs

print_table_size = False
counter = 0

def print_timing (func):
  def wrapper (*arg):
    t1 = time.time()
    res = func(*arg)
    t2 = time.time()
    print ("{}: {} ms".format(func.__name__, (t2 - t1) * 1000.0))
    return res

  return wrapper

def hirschberg_helper(A, B, depth):
  global counter
  counter += 1
  m = len(A)
  n = len(B)
  if n < 64 and m < 64:
    return lcs(A, B)
  if n == 0 or m == 0:
    return ("", 0)
    return
  if m == 1:
    match_index = B.find(A)
    if match_index != -1:
      return (A, 1)
    return ("", 0)
    return
  
  i = floor(m/2)
  k = find_k(A, B, i, depth)
  
  # if depth == 0:
  #   with Pool(2) as p:
  #     (seq1, lcs1), (seq2, lcs2) = p.starmap(hirschberg_helper, [(A[:i], B[:k], depth+1), (A[i:], B[k:], depth+1)])
  # else:
  (seq1, lcs1) = hirschberg_helper(A[:i], B[:k], depth+1)
  (seq2, lcs2) = hirschberg_helper(A[i:], B[k:], depth+1)

  return (seq1 + seq2, lcs1 + lcs2)

# @print_timing
def find_k(A, B, i, depth):
  sliced_A = A[:i]
  L = get_last_row_lcs_table(sliced_A, B, print_table_size)
  Lstar = get_last_row_lcs_table(sliced_A[::-1], B[::-1], print_table_size)
  sum_L = [x + y for x, y in zip(L, Lstar[::-1])]
  return sum_L.index(max(sum_L))

@print_timing
def hirschberg(A, B):
  return hirschberg_helper(A, B, 0)

def print_hirschberg(A, B):
  global counter
  counter = 0
  result = hirschberg(A, B)
  # print("".join(result[0])
  # print(result[1])
  print("counter: {}".format(counter))

if __name__ == "__main__":
  if len(sys.argv) == 3:
    print_hirschberg(sys.argv[1], sys.argv[2])
  else:
    print("usage: python edit_distance.py <string> <string>")
    exit(1)
