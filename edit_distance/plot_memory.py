import matplotlib.pyplot as plt
from scipy.interpolate import make_interp_spline, BSpline
import numpy as np 
import sys
import math

f = open("tablesizeH1000.txt", "r", encoding="UTF-16")
lst = f.readlines()
# plt.plot(range(len(lst)), [int(x.strip()) for x in lst])

# plt.xlabel('length of random DNA string') 
plt.xlabel('recursive call number') 

# xaxis= [100, 1000, 10000, 25000]
xaxis = list(range(len(lst)))

yaxis1=[25.25,38.06,1526.70,5303.05]
yaxis2=[26.01,27.08,27.81,28.18]

yaxis_table_1 = [91768,12003688,1560005840,5544759488]
yaxis_table_2 = [3320,42488,479096,720744]

yaxis_exec_1 = [4,449,50556,381344]
yaxis_exec_2 = [10,1038,108223,771429]

# used_y_axis1 = list(map(lambda x: math.log(x,10), yaxis_table_1))
# used_y_axis2 = list(map(lambda x: math.log(x,10), yaxis_table_2))

used_y_axis1 = [int(x.strip()) for x in lst]
used_y_axis2 = yaxis2
# used_y_axis1 = list(map(lambda x: x/(1024*1024), yaxis_table_1))
# used_y_axis2 = list(map(lambda x: x/(1024*1024), yaxis_table_2))
# used_y_axis1 = yaxis_exec_1
# used_y_axis2 = yaxis_exec_2

# plt.ylabel('highest memory usage in execution (mb)') 
# plt.title('Max memory usage of complete memo vs hirschberg lcs algorithm') 

plt.ylabel('memory of lcs table (mb)')
# plt.title('Max table size of complete memo vs hirschberg lcs algorithm')
plt.title('Max table size of hirschberg lcs algorithm across recursive calls')

# plt.ylabel('time (ms)')
# plt.title('Execution time of complete memo vs hirschberg lcs algorithm')

xnew = np.linspace(1, len(lst), 900) 
spl = make_interp_spline(xaxis, used_y_axis1, k=3)
# spl2 = make_interp_spline(xaxis, used_y_axis2, k=3)
smooth1 = spl(xnew)
# smooth2 = spl2(xnew)

# plt.plot(xaxis, used_y_axis1, 'rx')
# plt.plot(xaxis, used_y_axis2, 'bx')
plt.plot(xnew, smooth1, 'r')
# plt.plot(xnew, smooth2, 'b')
# plt.legend(['complete memo', 'hirschberg'], loc='upper left')
plt.legend(['hirschberg'], loc='upper left')
plt.show() 