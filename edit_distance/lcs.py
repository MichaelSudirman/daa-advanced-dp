import sys
import time
from pympler import asizeof

print_table_size = False

def print_timing (func):
  def wrapper (*arg):
    t1 = time.time()
    res = func(*arg)
    t2 = time.time()
    print ("{}: {} ms".format(func.__name__, (t2 - t1) * 1000.0))
    return res

  return wrapper

# @print_timing
def get_lcs_table(A, B):
  m = len(A)
  n = len(B)
  lcs_table = [[None]*(n+1) for i in range(m+1)]

  for i in range(m+1): 
      for j in range(n+1):
        if i == 0 or j == 0 : 
          lcs_table[i][j] = 0
        elif A[i-1] == B[j-1]:
          lcs_table[i][j] = lcs_table[i-1][j-1] + 1
        else:
          lcs_table[i][j] = max(lcs_table[i-1][j], lcs_table[i][j-1])

  if print_table_size:
    print("{}".format(asizeof.asizeof(lcs_table)))
  return lcs_table

# @print_timing
def get_last_row_lcs_table(A, B, will_print=False):
  m = len(A)
  n = len(B)
  lcs_table = [[None]*(n+1) for i in range(2)]

  for i in range(m+1): 
    for j in range(n+1):
      if i == 0 or j == 0 : 
        lcs_table[i%2][j] = 0
      elif A[i-1] == B[j-1]:
        lcs_table[i%2][j] = lcs_table[(i-1)%2][j-1] + 1
      else:
        lcs_table[i%2][j] = max(lcs_table[(i-1)%2][j], lcs_table[i%2][j-1])

  if will_print:
    print("{}".format(asizeof.asizeof(lcs_table)))
  return lcs_table[m%2]

def construct_sequence(A, B, lcs_table):
  i = len(A)
  j = len(B)
  sequence = [""]*lcs_table[-1][-1]
  seqI = len(sequence) - 1

  while (i != 0 and j != 0):
    current_cell = lcs_table[i][j]
    if A[i-1] == B[j-1]:
      sequence[seqI] = A[i-1]
      i -= 1
      j -= 1
      seqI -= 1
    elif lcs_table[i-1][j] > lcs_table[i][j-1]:
      i -= 1
    else:
      j -= 1
  
  return "".join(sequence)

# @print_timing
def lcs(A, B):
  lcs_table = get_lcs_table(A, B)
  sequence = construct_sequence(A, B, lcs_table)

  return (sequence, lcs_table[-1][-1])

def print_lcs(A, B):
  result = lcs(A, B)
  # print(result[0])
  # print(result[1])

if __name__ == "__main__":
  if len(sys.argv) == 3:
    print_edit_distance(sys.argv[1], sys.argv[2])
  else:
    print("usage: python edit_distance.py <string> <string>")
    exit(1)
