import sys
import string
import random
import time

from hirschberg import print_hirschberg
from lcs import print_lcs, get_last_row_lcs_table, get_lcs_table

def generate_id(size=6, chars=string.ascii_uppercase + string.digits):
  return ''.join(random.choice(chars) for _ in range(size))

if __name__ == "__main__":
  if sys.argv[1] == "g":
    size = int(sys.argv[2])
    A, B = generate_id(size=size), generate_id(size=size)
    f = open(sys.argv[3], "w")
    print("{} {}".format(A, B), file=f, end="")
  elif sys.argv[1] == "gdna":
    size = int(sys.argv[2])
    A, B = generate_id(size=size, chars="ATCG"), generate_id(size=size, chars="ATCG")
    f = open(sys.argv[3], "w")
    print("{} {}".format(A, B), file=f, end="")
  elif sys.argv[1] == "h":
    f = open(sys.argv[2], "r")
    A, B = f.readline().split(' ')
    print_hirschberg(A, B)
  elif sys.argv[1] == "l":
    f = open(sys.argv[2], "r")
    A, B = f.readline().split(' ')
    print_lcs(A, B)
  elif sys.argv[1] == "lsw":
    f = open(sys.argv[2], "r")
    A, B = f.readline().split(' ')
    print(get_last_row_lcs_table(A, B)[-1])
  elif sys.argv[1] == "compare":
    f = open("dna1000.txt", "r")
    A, B = f.readline().split(' ')
    for i in range(10):
      print_hirschberg(A, B)
      time.sleep(2)
  elif sys.argv[1] == "process":
    f = open("test.txt", "r")
    for line in f.readlines():
      words = line.split(' ')
      print(words)
      result = ["", 0, 0]
      if words[0].equals(">"):
        print("{}  avg: {}", result, result[1]/max(result[2], 1))
        result = [words[1], 0, 0]
      elif words[0] == "hirschberg:":
        result[1] = float(words[1])
        result[2] += 1
  