import time
import sys
from nltk.corpus import stopwords
def lcs(X , Y): 
    # find the length of the strings 
    m = len(X) 
    n = len(Y) 
  
    # declaring the array for storing the dp values 
    L = [[None]*(n+1) for i in range(m+1)] 
  
    """Following steps build L[m+1][n+1] in bottom up fashion 
    Note: L[i][j] contains length of LCS of X[0..i-1] 
    and Y[0..j-1]"""
    for i in range(m+1): 
        for j in range(n+1): 
            if i == 0 or j == 0 : 
                L[i][j] = 0
            elif X[i-1] == Y[j-1]: 
                L[i][j] = L[i-1][j-1]+1
            else: 
                L[i][j] = max(L[i-1][j] , L[i][j-1]) 

    # print(len(L))  
    # print(sum(len(x) for x in L))
    # print(sys.getsizeof(L))
    # L[m][n] contains the length of LCS of X[0..n-1] & Y[0..m-1] 
    return L[m][n] 

#end of function lcs 
  
  
# Driver program to test the above function 

# list1 = list("ABCDEFGHIJKLMNO~!@#$%^&")
# list2 = list("AB1234567890*()_+PQRSTUVWXYZ")
# list1 = "we are not together".split(" ")
# list2 = "they were not in the same line".split(" ")

# f1 = open("note1.txt","r")
# f2 = open("note2.txt","r")
# list1 = f1.read().split()
# list2 = f2.read().split()
 
 
stopWords = stopwords.words('english')

list1 = "Long ago, when there was no written history, these islands were the home of millions of happy birds; the resort of a hundred times more millions of fishes, sea lions, and other creatures. Here lived innumerable creatures predestined from the creation of the world to lay up a store of wealth for the British farmer, and a store of quite another sort for an immaculate Republican government."
list2 = "In ages which have no record these islands were the home of millions of happy birds, the resort of a hundred times more millions of fishes, of sea lions, and other creatures whose names are not so common; the marine residence, in fact, of innumerable creatures predestined from the creation of the world to lay up a store of wealth for the British farmer, and a store of quite another sort for an immaculate Republican government."
list1,list2 = list1.split(),list2.split()

list1 = [x for x in list1 if not x in stopWords]
list2 = [x for x in list2 if not x in stopWords]
# list1 = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
# list2 = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"

# list1 = list("ABCDEFGHIJKLMNO~!@#$%^&")
# list2 = list("AB1234567890*()_+PQRSTUVWXYZ")
start_time = time.time()
print ("Length of LCS is ", lcs(list1,list2) )
print("--- %s seconds ---" % (time.time() - start_time))