import time
def findMatches(firstStr,secondStr):
    A=firstStr
    B=secondStr
    m = len(A) 
    n = len(B)
    I = [x for x in range(m)]
    J = [x for x in range(n)]
    A,I = zip(*sorted(zip(A,I)))
    B,J = zip(*sorted(zip(B,J)))
    # A,B,I,J = list(A),list(B),list(I),list(J)

    cnt = 0
    i,j = 0,0
    while(i < m and j < n):
        if(A[i] < B[j]):
            i += 1
        elif(A[i] > B[j]):
            j += 1
        else:
            ii = i
            while(ii < len(A) and A[ii] == A[i]):
                jj = j
                while(jj < len(B) and B[jj] == B[j]):
                    # report(I[ii],J[jj])
                    matchList.append([I[ii],J[jj]])
                    jj += 1
                    
                ii += 1
            i = ii
            j = jj
            
# def report(x,y): 
#     matchList.append([x,y])

def sparseLCSFinal(matchList):
    size = len(matchList)
    # matchList.add([0,0])
    if(not len(matchList)):
        print(0)
        return
    opt = [1] * size
    largest = 1
    # print(len(opt))
    for i in range(1,size):
        k = matchList[i]
        for j in range(i):
            n = matchList[j]
            if(k[0] > n[0] and k[1] > n[1]):
                opt[i] = max(opt[i], opt[j] + 1)
                if(opt[i] > largest):
                    largest = opt[i]
    print(largest)
    # print(opt[size-1])
    print(opt)

list1 = "AAABCDEFGHIJKLMNO~!@#$%^&"
list2 = "AABZAAA1234567890*()_+PQRSTUVWXYZ"
# list1 = "AABZZC"
# list2 = "ABCBAZ"
# f1 = open("note1.txt","r")
# f2 = open("note2.txt","r")
# f1 = open("note3b.txt","r")
# f2 = open("note4.txt","r")
# list1 = f1.read().split()
# list2 = f2.read().split()
# list1 = "we are not together".split(" ")
# list2 = "they were not in the same line".split(" ")
 
valList = []
matchList = []

start_time = time.time()
findMatches(list1,list2)
matchList.sort()
# for i in range(len(matchList)):
#     for j in range(len(matchList[i])):
#         print("{}".format(matchList[i][j]),end = "-")
#     print("")
sparseLCSFinal(matchList)
print("--- %s seconds ---" % (time.time() - start_time))
print(matchList)