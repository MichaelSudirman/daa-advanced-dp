import time
import sys
from nltk.corpus import stopwords

def findMatches(firstStr,secondStr):
    matchList = []
    A=firstStr
    B=secondStr
    m = len(A) 
    n = len(B)
    I = [x for x in range(m)]
    J = [x for x in range(n)]
    A,I = zip(*sorted(zip(A,I)))
    B,J = zip(*sorted(zip(B,J)))

    cnt = 0
    i,j = 0,0
    while(i < m and j < n):
        if(A[i] < B[j]):
            i += 1
        elif(A[i] > B[j]):
            j += 1
        else:
            ii = i
            while(ii < len(A) and A[ii] == A[i]):
                jj = j
                while(jj < len(B) and B[jj] == B[j]):
                    matchList.append([I[ii],J[jj]])
                    jj += 1
                    
                ii += 1
            i = ii
            j = jj
    matchList.sort()
    
    return matchList

# UNIT OF OPT [lcs length, previous max matchpoint, index in list1]
def sparseLCS(list1,list2):
    matchList = findMatches(list1,list2)
    if(not len(matchList)):
        return 0
    size = len(matchList)
    # opt = [1] * size
    opt = [[1, 0, 0]] * size
    # largest = 1
    largest = [1, None, None]
    for i in range(1,size):
        k = matchList[i]
        for j in range(i):
            n = matchList[j]
            if(k[0] > n[0] and k[1] > n[1]):
                # opt[i] = max(opt[i], opt[j] + 1)
                if opt[i][0] < opt[j][0] + 1:
                    opt[i] = [opt[j][0] + 1, j, k[0]]
                # if(opt[i] > largest):
                #     largest = opt[i]
                if(opt[i][0] > largest[0]):
                    largest = opt[i]
    # print(opt)
    sequence = [None] * largest[0]
    current = largest
    for i in range(largest[0]):
        sequence[-(i+1)] = list1[current[2]]
        current = opt[current[1]]
    print("seq {}".format(" ".join(sequence)))
    return largest

f1 = open("note1.txt","r")
f2 = open("note2.txt","r")
list1 = f1.read().split()
list2 = f2.read().split()

# list1 = "Long ago, when there was no written history, these islands were the home of millions of happy birds; the resort of a hundred times more millions of fishes, sea lions, and other creatures. Here lived innumerable creatures predestined from the creation of the world to lay up a store of wealth for the British farmer, and a store of quite another sort for an immaculate Republican government."
# list2 = "In ages which have no record these islands were the home of millions of happy birds, the resort of a hundred times more millions of fishes, of sea lions, and other creatures whose names are not so common; the marine residence, in fact, of innumerable creatures predestined from the creation of the world to lay up a store of wealth for the British farmer, and a store of quite another sort for an immaculate Republican government."
# list1,list2 = list1.split(),list2.split()

stopWords = stopwords.words('english')
list1 = [x for x in list1 if not x in stopWords]
list2 = [x for x in list2 if not x in stopWords]

start_time = time.time()
print ("Length of LCS is ", sparseLCS(list1,list2) )
print("--- %s seconds ---" % (time.time() - start_time))
